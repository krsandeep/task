package com.app.sandeep.repo;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.sandeep.dto.Assignments;

public interface AssignmentRepo extends JpaRepository<Assignments, Integer> {
	
	public Set<Assignments>findByStudentName(String name);
	public Set<Assignments>findBySubject(String subject);

}
