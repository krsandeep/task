package com.app.sandeep.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.sandeep.dto.Distributions;

public interface DistributionRepo extends JpaRepository<Distributions, String> {

}
