package com.app.sandeep.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Assignments {
	@Id
	private Integer serialNo;
	private String studentName;
	private String subject;
	private String assignmentCategory;
	private String dateOfSubmission;
	private Integer points;
	public Assignments() {
		super();
	}
	public Assignments(Integer serialNo, String studentName, String subject, String assignmentCategory,
			String dateOfSubmission, Integer points) {
		super();
		this.serialNo = serialNo;
		this.studentName = studentName;
		this.subject = subject;
		this.assignmentCategory = assignmentCategory;
		this.dateOfSubmission = dateOfSubmission;
		this.points = points;
	}
	public Integer getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getAssignmentCategory() {
		return assignmentCategory;
	}
	public void setAssignmentCategory(String assignmentCategory) {
		this.assignmentCategory = assignmentCategory;
	}
	public String getDateOfSubmission() {
		return dateOfSubmission;
	}
	public void setDateOfSubmission(String dateOfSubmission) {
		this.dateOfSubmission = dateOfSubmission;
	}
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	@Override
	public String toString() {
		return "Assignments [serialNo=" + serialNo + ", studentName=" + studentName + ", subject=" + subject
				+ ", assignmentCategory=" + assignmentCategory + ", dateOfSubmission=" + dateOfSubmission + ", points="
				+ points + "]";
	}
	
	
	

}
